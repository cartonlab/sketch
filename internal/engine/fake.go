package engine

import (
	"github.com/go-xorm/xorm"
)

// FAKE - fake DB.
type FAKE struct {
}

// Client - функция заглушка
func (f *FAKE) Client() *xorm.Engine {
	return nil
}

// StoreLogs - функция заглушка
func (f *FAKE) StoreLogs() {
}

// IsAlive - функция заглушка
func (f *FAKE) IsAlive() bool {
	return true
}

// NewFAKE - функция новый экземпляр
func NewFAKE() *FAKE {
	f := FAKE{}
	return &f
}
