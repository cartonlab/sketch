package engine

import (
	"time"

	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	"github.com/rs/zerolog/log"

	"gitlab.com/cartonlab/sketch/internal/config"
	"gitlab.com/cartonlab/sketch/internal/metrics"
	"gitlab.com/cartonlab/sketch/internal/models"
	"gitlab.com/cartonlab/sketch/internal/utils"
)

// DB - интерфейс для доступа к БД
var DB DBInterface

// DBInterface - Методы БД.
type DBInterface interface {
	Client() *xorm.Engine
	StoreLogs()
	IsAlive() bool
}

// Engine - движок БД.
type Engine struct {
	client *xorm.Engine
}

// PG - БД.
type PG struct {
	Engine
	connectionString string
	isAlive          bool
	isLogging        bool
	appLogValid      time.Duration
	appLogEnabled    bool
}

// NewPG - функция
func NewPG(arg ...interface{}) *PG {
	pg := PG{
		connectionString: arg[0].(string),
		appLogValid:      time.Duration(7),
		appLogEnabled:    true,
	}

	if len(arg) > 1 {
		if cfg, ok := arg[1].(*config.Config); ok {
			pg.appLogEnabled = cfg.Bool("LOG_DB_ENABLED")
			pg.appLogValid = time.Duration(cfg.Int("LOG_DB_VALID"))
		}
	}

	pg.connect()
	pg.keepConnect()

	log.Info().Msg("Параметры подключения к БД: " + pg.connectionString)

	return &pg
}

// StoreLogs - запуск функции записи логов zerolog в БД
func (pg *PG) StoreLogs() {
	if pg.isLogging {
		return
	}

	pg.storeLogs()
	pg.isLogging = true
	pg.freezeLogs()
}

// storeLogs - функция записи логов zerolog в БД
func (pg *PG) storeLogs() {

	if !pg.appLogEnabled {
		go func() {
			for {
				// Если отключено логироване в БД просто очищаем канал
				<-Logs
			}
		}()
		return
	}

	go func() {
		for {
			if pg.Engine.client == nil || !pg.isAlive {
				continue
			}
			// Если включено логироване в БД забираем сообщения из канала и записываем их в БД
			record := models.PGLogger{Log: <-Logs}
			pg.Engine.client.Insert(&record)
			// ВНИМАНИЕ!
			// Воизбежание реккурсивногй записи в БД любое логирование в этой фукции недопустимо!
			// Обработка ошибок не нужна.
		}
	}()
}

// freezeLogs - функция очистки логов zerolog в БД
func (pg *PG) freezeLogs() {
	if !pg.appLogEnabled {
		return
	}

	records := models.PGLogger{}

	// Из базы будут удаляться записи старше N суток
	expiredAt := time.Duration(pg.appLogValid * 24 * time.Hour)

	ticker := time.NewTicker(1 * time.Second)
	go func() {
		for {
			<-ticker.C

			if pg.Engine.client == nil || !pg.isAlive {
				continue
			}
			pg.Engine.client.Where("created_at < ?", time.Now().Add(-expiredAt).Format(utils.FormatTimestampTZ)).Delete(&records)

			// Метрики Prometheus. Количество записей в таблице с логами
			logs, _ := pg.Engine.client.Count(&records)
			metrics.PrometheusRegisterDBLogsLength.Set(float64(logs))
		}
	}()
}

// keepConnect - функция поддерживает подключение к БД
func (pg *PG) keepConnect() {
	initInterval := 250
	interval := 250
	initLongInterval := 4000

	ticker := time.NewTicker(1 * time.Second)
	go func() {
		for {
			<-ticker.C
			if !pg.ping() {
				if interval <= initLongInterval {
					interval = interval + initInterval
				}
				log.Info().Msg("Выполняется переподключение к БД.....")

				if pg.isAlive {
					pg.close()
					pg.connect()
				}
			} else {
				interval = initInterval
			}

			ticker.Stop()
			ticker = time.NewTicker(time.Duration(interval) * time.Millisecond)
		}
	}()
}

// Client Возвращает клиента для выполнения запросов
func (pg *PG) Client() *xorm.Engine {
	return pg.Engine.client
}

// connect подключение к СУБД
func (pg *PG) connect() {
	var e error

	pg.Engine.client, e = xorm.NewEngine("postgres", pg.connectionString)
	if e != nil {
		log.Error().Msg("Подключение к БД не успешно: " + e.Error())
		pg.isAlive = false
	} else {
		log.Info().Msg("Подключение к БД успешно.")
		pg.isAlive = true
	}
}

// close закрывает подключение к СУБД.
func (pg *PG) close() {
	if pg.Engine.client == nil {
		pg.isAlive = false
		return
	}

	pg.Engine.client.Close()
}

// IsAlive Показывает доступнсть БД
func (pg *PG) IsAlive() bool {
	return pg.isAlive
}

// Ping проверяет наличие подключения к СУБД.
func (pg *PG) ping() bool {
	if pg.Engine.client == nil {
		pg.isAlive = false
		return pg.isAlive
	}

	// "Гасим" вывод PING в лог
	logCurrentLevel := pg.Engine.client.Logger().Level()
	pg.Engine.client.Logger().SetLevel(core.LOG_OFF)
	e := pg.Engine.client.Ping()
	pg.Engine.client.Logger().SetLevel(logCurrentLevel)

	if e != nil {
		log.Error().Msg(e.Error())
		pg.isAlive = false
	} else {
		pg.isAlive = true
	}

	return pg.isAlive
}
