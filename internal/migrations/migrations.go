package migrations

import (
	"github.com/go-xorm/xorm"

	"gitlab.com/cartonlab/sketch/internal/engine/migrate"
)

var (
	// Migrations все миграции приложения
	Migrations = []*migrate.Migration{
		{
			ID: "201807190001",
			Migrate: func(engine *xorm.Engine) (e error) {
				_, e = engine.Exec(`
					CREATE TABLE "logs" (
					    "id"         bigserial PRIMARY KEY,
					    "created_at" timestamp with time zone,
					    "log"        jsonb
					);

					CREATE INDEX "logs_created_at_timestamptz_btree_idx" ON "logs" ( ("created_at"::timestamptz) );
					CREATE INDEX "logs_log_level_text_btree_idx"         ON "logs" ( (("log"->>'level')::text) );
					CREATE INDEX "logs_log_caller_text_btree_idx"        ON "logs" ( (("log"->>'caller')::text) );
				`)

				return
			},
			Rollback: func(engine *xorm.Engine) (e error) {
				_, e = engine.Exec(`DROP TABLE "logs"`)
				return
			},
		},
	}
)
