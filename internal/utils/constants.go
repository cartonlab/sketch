package utils

// FormatTimestamp - Формат таймстемпа без таймзоны
const (
	FormatTimestamp   = "2006-01-02 15:04:05.999999"
	FormatTimestampTZ = "2006-01-02 15:04:05.999999Z0700"
)
