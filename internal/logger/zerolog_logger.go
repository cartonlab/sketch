package logger

import (
	"os"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/cartonlab/sketch/internal/engine"
	"gitlab.com/cartonlab/sketch/internal/utils"
)

var (
	// Global - глобальный объект логгера
	Global = Logger{
		logLevel:              zerolog.InfoLevel,
		deferredLevel:         zerolog.DebugLevel,
		deferredLevelDuration: 30 * time.Minute,
		deferredLevelStopTime: time.Now(),
	}
)

// Logger - логгер
type Logger struct {
	zerolog.Logger

	logLevel              zerolog.Level // Базовый уровень логирования
	deferredLevel         zerolog.Level // Временный уровень логирования
	deferredLevelDuration time.Duration // Длительность действия временного уровня логирования
	deferredLevelStopTime time.Time
	deferredWatch         bool
}

// InitLogger logger
func InitLogger(config ...interface{}) *Logger {
	var level string

	if len(config) > 0 {
		level, _ = config[0].(string)
	}

	switch strings.ToLower(level) {
	case "error":
		Global.logLevel = zerolog.ErrorLevel
	case "debug":
		Global.logLevel = zerolog.DebugLevel
	default:
		Global.logLevel = zerolog.InfoLevel
	}

	zerolog.SetGlobalLevel(Global.logLevel)

	Global.Logger = zerolog.New(Global).With().Caller().Logger()

	log.Logger = Global.Logger

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Info().Msg("Установлен уровень логирования: " + Global.logLevel.String())
	zerolog.SetGlobalLevel(Global.logLevel)

	return &Global
}

// Write - Врапер для обработки логов
func (g Logger) Write(b []byte) (int, error) {
	select {
	// Записывем лог в канал из которого будем писать в БД
	case engine.Logs <- string(b):
	default:
	}

	// Отправляем олг в STDOUT
	return os.Stdout.Write(b)
}

// EnableDeferredLevel - Установить уровень логирования на время
func (g *Logger) EnableDeferredLevel() {
	if g.Level() == g.deferredLevel {
		g.deferredLevelStopTime = time.Now().Add(g.deferredLevelDuration)
		log.Info().Msg("Уровень логирования " + g.deferredLevel.String() + " продлён до " + g.deferredLevelStopTime.Format(utils.FormatTimestampTZ))
		return
	}

	g.setLevel(g.deferredLevel)
	go g.watchDeferred()
}

// DisableDeferredLevel - Вернуть уровень логирования на базовый
func (g *Logger) DisableDeferredLevel() {
	g.deferredWatch = false

	if g.Level() != g.logLevel {
		g.setLevel(g.logLevel)
	}
}

func (g *Logger) watchDeferred() {
	g.deferredWatch = true
	g.deferredLevelStopTime = time.Now().Add(g.deferredLevelDuration)

	ticker := time.NewTicker(1 * time.Second)
	for t := range ticker.C {
		if !g.deferredWatch {
			return
		}

		if g.deferredLevelStopTime.Before(t) {
			g.DisableDeferredLevel()
			return
		}
	}
}

func (g *Logger) setLevel(level zerolog.Level) {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	log.Info().Msg("Установлен уровень логирования: " + level.String())
	zerolog.SetGlobalLevel(level)
}

// Level - Текущий уровень логирования
func (g *Logger) Level() zerolog.Level {
	return zerolog.GlobalLevel()
}
