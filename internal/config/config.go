package config

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Config - структура конфига
type Config map[string]interface{}

// New - новый конфиг из чего-то
func New(conf ...map[string]interface{}) *Config {
	c := make(Config)

	for i := range conf {
		for name := range conf[i] {
			c[name] = conf[i][name]
		}
	}

	c.overrideENV()

	return &c
}

// Stringify - Возвращает конфиг строкой
func (c *Config) Stringify() string {
	order := []string{}
	print := ""

	for name := range *c {
		order = append(order, name)
	}

	sort.Strings(order)

	for _, name := range order {
		print += fmt.Sprintf("%s=%v\n", name, (*c)[name])
	}

	return print
}

// overrideENV - Переопределяет значения конфига переменными окружения
func (c *Config) overrideENV() *Config {
	for name := range *c {
		if value := os.Getenv(name); value != "" {

			// Проверяем можно ли строку преобразовать в слайс с помощью разделителя
			if maybeSlice := strings.Split(value, "|"); len(maybeSlice) > 1 {
				(*c)[name] = maybeSlice
				continue
			}
			(*c)[name] = value
		}
	}

	return c
}

// Int - Возвращает int по ключу
func (c *Config) Int(name string) int {

	fres, ok := ((*c)[name]).(float64)
	if ok {
		return int(fres)
	}

	res, ok := ((*c)[name]).(int)
	if ok {
		return res
	}

	sres, ok := ((*c)[name]).(string)
	if ok {
		i, e := strconv.Atoi(sres)
		if e == nil {
			return i
		}
		return 0
	}

	bres, ok := ((*c)[name]).(bool)
	if ok && bres {
		return 1
	}

	return 0
}

// String - Возвращает string по ключу
func (c *Config) String(name string) string {
	res, ok := ((*c)[name]).(string)
	if ok {
		return res
	}

	ires, ok := ((*c)[name]).(int)
	if ok {
		return strconv.Itoa(ires)
	}

	bres, ok := ((*c)[name]).(bool)
	if ok {
		if bres {
			return "true"
		}
		return "false"
	}

	return ""
}

// Bool - Возвращает bool по ключу
func (c *Config) Bool(name string) bool {
	b, ok := ((*c)[name]).(bool)
	if ok {
		return b
	}

	i, ok := ((*c)[name]).(int)
	if ok {
		return i != 0
	}

	f, ok := ((*c)[name]).(float64)
	if ok {
		return f != 0
	}

	s, ok := ((*c)[name]).(string)
	if ok {
		switch s {
		case "", "false", "FALSE":
			return false
		}

		i, e := strconv.Atoi(s)
		if e == nil {
			return i != 0
		}
		return true
	}

	return false
}

// Raw - Возвращает нечто неизвестное по ключу
func (c *Config) Raw(name string) interface{} {
	res, _ := ((*c)[name])
	return res
}

// SliceString - Возвращает слайс string по ключу
func (c *Config) SliceString(name string) []string {
	res, _ := ((*c)[name]).([]string)
	return res
}

// SliceInt - Возвращает слайс int по ключу
func (c *Config) SliceInt(name string) []int {
	res, _ := ((*c)[name]).([]int)
	return res
}
