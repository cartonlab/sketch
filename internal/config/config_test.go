package config

import (
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestOverrideENV(t *testing.T) {
	result1 := "1"
	fixture1 := "4"

	fixture2 := "4|5"
	fixture3 := "4|5|6"
	result2 := []string{"4", "5"}

	os.Setenv("test", result1)

	cfg := New(map[string]interface{}{"test": fixture1})

	if answ := cfg.String("test"); !cmp.Equal(answ, result1) {
		t.Errorf(`config.TestOverrideENV FAILED %v != %v`, answ, result1)
	}

	os.Setenv("test", fixture2)
	cfg = New(map[string]interface{}{"test": fixture3})

	answ := cfg.SliceString("test")
	if !cmp.Equal(answ, result2) {
		t.Errorf(`config.TestOverrideENV FAILED %v != %v`, answ, result2)
	}
}

func TestStringify(t *testing.T) {
	os.Setenv("test", "18")
	fixture := map[string]interface{}{"test": "12", "test_2": true}

	cfg := New(fixture)

	if cfg.Stringify() != "test=18\ntest_2=true\n" {
		t.Errorf(`config.Print FAILED`)
	}
}

func TestBool(t *testing.T) {
	cfg := New()
	fixtures := map[interface{}]bool{
		true:     true,
		false:    false,
		0:        false,
		1:        true,
		3.14:     true,
		"":       false,
		"false":  false,
		"FALSE":  false,
		"1":      true,
		"true":   true,
		"TRUE":   true,
		"ok":     true,
		int64(1): false,
	}

	for fixture, result := range fixtures {
		(*cfg)["test"] = fixture
		if answ := cfg.Bool("test"); !cmp.Equal(answ, result) {
			t.Errorf(`config.Bool("%v") = '%v'. It does not match '%v'`, fixture, answ, result)
		}
	}
}

func TestInt(t *testing.T) {
	cfg := New()
	fixtures := map[interface{}]int{
		10:       10,
		0:        0,
		-10:      -10,
		"0":      0,
		"15":     15,
		"-15":    -15,
		"ddd":    0,
		"":       0,
		true:     1,
		false:    0,
		12.23:    12,
		int64(4): 0,
	}

	for fixture, result := range fixtures {
		(*cfg)["test"] = fixture
		if answ := cfg.Int("test"); !cmp.Equal(answ, result) {
			t.Errorf(`config.Int(%v) = %v. It does not match %v`, fixture, answ, result)
		}
	}
}

func TestString(t *testing.T) {
	cfg := New()
	fixtures := map[interface{}]string{
		15:       "15",
		0:        "0",
		-15:      "-15",
		true:     "true",
		false:    "false",
		"habra":  "habra",
		"":       "",
		int64(4): "",
	}

	for fixture, result := range fixtures {
		(*cfg)["test"] = fixture
		if answ := cfg.String("test"); !cmp.Equal(answ, result) {
			t.Errorf(`config.String("%v") = "%v". It does not match "%v"`, fixture, answ, result)
		}
	}
}

func TestSliceString(t *testing.T) {
	cfg := New()
	fixture := []string{"a", "b"}
	(*cfg)["test"] = fixture

	answ := cfg.SliceString("test")

	if !cmp.Equal(answ, fixture) {
		t.Errorf(`config.SliceString("%v") = "%v". It does not match "%v"`, fixture, answ, fixture)
	}
}

func TestSliceInt(t *testing.T) {
	cfg := New()
	fixture := []int{12, -28, 0}
	(*cfg)["test"] = fixture

	answ := cfg.SliceInt("test")

	if !cmp.Equal(answ, fixture) {
		t.Errorf(`config.SliceInt("%v") = "%v". It does not match "%v"`, fixture, answ, fixture)
	}
}

func TestRaw(t *testing.T) {
	type MyTYPE int

	cfg := New()
	fixture := []MyTYPE{12, -28, 0}
	(*cfg)["test"] = fixture

	pre := cfg.Raw("test")
	answ := pre.([]MyTYPE)

	if !cmp.Equal(answ, fixture) {
		t.Errorf(`config.SliceInt("%v") = "%v". It does not match "%v"`, fixture, answ, fixture)
	}
}
