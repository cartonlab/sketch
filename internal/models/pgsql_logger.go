package models

import (
	"time"
)

// PGLogger - Модель хранения логов в БД
type PGLogger struct {
	ID        int64       `xorm:"'id' pk autoincr BIGINT"`
	CreatedAt time.Time   `xorm:"'created_at' created TIMESTAMPZ"`
	Log       interface{} `xorm:"'log' JSONB DEFAULT NULL"`
}

// TableName - название таблицы в БД
func (l *PGLogger) TableName() string {
	return "logs"
}
