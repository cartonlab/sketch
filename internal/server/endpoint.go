package server

import (
	"net/http"
	"sync/atomic"

	"gitlab.com/cartonlab/sketch/internal/metrics"
)

// Endpoint эндоинт.
type Endpoint struct {
	Name        string   // Имя ендпоинта.
	Description string   // Краткое описане ендпоинта.
	Path        string   // URI Ендпоинта - не должен быть пустым. Иначе не добавиться при инициализации.
	Methods     []string // HTTP Методы, которые обслуживает ендпоинт.
	Handler     Handler  // HTTP обработчик
}

// Handler Обработчик ендпоинта.
type Handler func(http.ResponseWriter, *http.Request)

// ServeHTTP Вызов обработчика HTTP запроса.
func (endpoint Endpoint) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Вызов обработчика HTTP запросов
	if endpoint.Handler != nil {
		endpoint.Handler(w, r)
		atomic.AddInt64(&metrics.PrometheusRegisterHTTPRequests, 1)
	}
}
