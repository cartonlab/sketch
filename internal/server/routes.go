package server

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/cartonlab/sketch/internal/server/handlers"
)

var (
	// Routes - Роуты - коллекция ендпоинтов для роутера веб сервера
	Routes = []Endpoint{
		Endpoint{
			Description: "Простой ответ от сервера. Может быть использован для проверки жив - не жив.",
			Path:        "/alive",
			Methods:     []string{"GET"},
			Handler:     handlers.Alive,
		},
		Endpoint{
			Description: "Метрики",
			Path:        "/metrics",
			Methods:     []string{"GET"},
			Handler:     promhttp.Handler().ServeHTTP,
		},
		Endpoint{
			Description: "Временно включает debug логирование.",
			Path:        "/debug/{enable}",
			Methods:     []string{"GET"},
			Handler:     handlers.Debug,
		},
	}
)
