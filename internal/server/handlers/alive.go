package handlers

import (
	"net/http"

	"gitlab.com/cartonlab/sketch/internal/engine"
)

// Alive - функция
func Alive(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("HTTP OK\n"))

	if engine.DB.IsAlive() {
		w.Write([]byte("DB CONNECTION OK\n"))
	} else {
		w.Write([]byte("DB CONNECTION DOWN\n"))
	}
}
