package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/cartonlab/sketch/internal/logger"
)

// Debug - функция включение - отключение Debug логирования
func Debug(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	switch vars["enable"] {
	case "enable":
		logger.Global.EnableDeferredLevel()
	default:
		logger.Global.DisableDeferredLevel()
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(logger.Global.Level().String() + " enabled"))
}
