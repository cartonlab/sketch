package server

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Router - роутер вебсервера
type Router struct {
	mux *mux.Router
}

// NewRouter Создать - новый роутер
func NewRouter(endpoints []Endpoint) *Router {
	rt := Router{
		mux: mux.NewRouter().StrictSlash(true),
	}

	for i := range endpoints {
		rt.mux.Methods(endpoints[i].Methods...).Name(endpoints[i].Name).Path(endpoints[i].Path).Handler(endpoints[i])
	}

	return &rt
}

// ServeHTTP Вызов обработчика HTTP запроса.
func (rt *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rt.mux.ServeHTTP(w, r)
}
