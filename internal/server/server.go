package server

import (
	"net/http"

	"github.com/rs/zerolog/log"
)

// Serve Запуск HTTP сервера.
func Serve(init string) error {
	log.Info().Msg("Сервер запущен на " + init)
	e := http.ListenAndServe(init, NewRouter(Routes))
	if e != nil {
		log.Fatal().Err(e).Msg("Запуск не получился")
	}
	return e
}
