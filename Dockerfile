FROM registry.gitlab.com/cartonlab/sketch/image:for-build-golang-1.10.3-alpine3.7

COPY sketch /opt/service

EXPOSE 8080

ENTRYPOINT ["/opt/service"]
