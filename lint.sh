#!/bin/bash

LIST=$(go list ./... | grep -v "vendor")

gofmt -w $(find ./ | grep -v "./vendor" | grep -P .*\.go$);
echo "gofmt DONE";

golint ${LIST};
echo "golint DONE";

gosimple ${LIST};
echo "gosimple DONE";

staticcheck ${LIST};
echo "staticcheck DONE";

go test -cover ${LIST};
echo "go test DONE";
