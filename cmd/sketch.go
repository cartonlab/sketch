package main

import (
	"fmt"

	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"

	"gitlab.com/cartonlab/sketch/internal/config"
	"gitlab.com/cartonlab/sketch/internal/engine"
	"gitlab.com/cartonlab/sketch/internal/engine/migrate"
	"gitlab.com/cartonlab/sketch/internal/logger"
	"gitlab.com/cartonlab/sketch/internal/metrics"
	"gitlab.com/cartonlab/sketch/internal/migrations"
	"gitlab.com/cartonlab/sketch/internal/server"
)

var (
	defaults = config.Config{
		"HTTP_HOST":      "",
		"HTTP_PORT":      "8080",
		"LOG_LEVEL":      "info",
		"LOG_DB_ENABLED": "1",
		"LOG_DB_VALID":   "2", // В время жизни записией логов в БД (в днях)
		"PG_CONNECT":     `user='service' password='service' host='localhost' port=5432 dbname='!service' sslmode='disable'`,
	}
)

// doMigrations Запуск миграций приложения
func doMigrations() {
	log.Debug().Msg("Делаются миграции БД")

	options := migrate.DefaultOptions
	options.TableName = "migrations"

	m := migrate.New(engine.DB.Client(), options, migrations.Migrations)
	e := m.Migrate()
	if e != nil {
		log.Error().Msg(e.Error())
	}

	log.Debug().Msg("Миграции БД завершены")
}

func main() {
	cfg := config.New(defaults)

	metrics.InitPrometheus()

	configString := "Конфиг:\n" + cfg.Stringify()
	fmt.Println(configString)

	logger.InitLogger(cfg.String("LOG_LEVEL"))

	log.Info().Msg(configString)

	engine.DB = engine.NewPG(cfg.String("PG_CONNECT"))

	doMigrations()

	engine.DB.StoreLogs()

	server.Serve(cfg.String("HTTP_HOST") + ":" + cfg.String("HTTP_PORT"))
}
